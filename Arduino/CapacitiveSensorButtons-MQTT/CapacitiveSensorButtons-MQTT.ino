/*
 * needed for this to work is the kingslamp noodl project
 * only turns on the lamp on touch
 */

#include <CapacitiveSensor.h>
CapacitiveSensor   cs_1_2 = CapacitiveSensor(1, 2);       // 1M resistor between pins 1 & 2, pin 2 is sensor pin, add a wire and or foil if desired

boolean oldtouch = HIGH;

unsigned long previousMillis = 0;
const long interval = 1000;

int bri;

#include <WiFi101.h>
#include <MQTTClient.h>
#include <ArduinoJson.h>

const char *wifi_ssid = "NETGEAR32"; //replace with your wifi name
const char *wifi_pass = "stronghat423"; //replace with your wifi password

const char *mqtt_broker = "10.0.0.3"; //replace with the IP of the computer running Noodl
const char *mqtt_name = "My Arduino"; //You can change this to be anything

const char *mqtt_id = ""; //token key/username. Noodl doesn't require authentication so leave this blank
const char *mqtt_password = ""; //token secret/password. Noodl doesn't require authentication so leave this blank

WiFiClient wifi_client; //Tip: replace this with WiFiSSLClient if you use another broker that requires SSL
MQTTClient mqtt_client;

void messageReceived(String& topic, String& payload);

void connectToWifi() {
  Serial.print("connecting wifi...");

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");
}

void connectToBroker() {
  Serial.print("\nconnecting mqtt...");
  while (!mqtt_client.connect(mqtt_name, mqtt_id, mqtt_password)) {
    Serial.print(".");
  }

  Serial.println("\nconnected!");
}


void setup() {
  Serial.begin(115200);

  WiFi.begin(wifi_ssid, wifi_pass);
  mqtt_client.begin(mqtt_broker, wifi_client);
  mqtt_client.onMessage(messageReceived);

  cs_1_2.set_CS_AutocaL_Millis(20000);
}

void loop()
{
  if (!mqtt_client.connected()) {
    connectToWifi();
    connectToBroker();
    mqtt_client.subscribe("arduino");
  }

  //if there's an incoming message, the messageReceived callback function will be called
  mqtt_client.loop();

  long touch =  cs_1_2.capacitiveSensor(30);

  unsigned long currentMillis = millis();


  //    Serial.println( touch );

  if (touch < 16000 && oldtouch == HIGH) {
    oldtouch = LOW;
    Serial.print( touch );
    Serial.println(" I am not being touched anymore");
    mqtt_client.publish("arduino", "{\"high\": 0}");
    bri = 0;
  }

  if (touch >= 16000 && oldtouch == LOW) {
    oldtouch = HIGH;
    Serial.print( touch );
    Serial.println(" I have been touched");
    mqtt_client.publish("arduino", "{\"high\": 1}");
  }

  if (touch >= 16000 && oldtouch == HIGH) {
    
    oldtouch = HIGH;
    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      String payload = "{\"bri\": " ;
      String einde = "}" ;
      String message = payload + bri + einde;
      mqtt_client.publish("arduino", message );
      Serial.print( touch );
      Serial.println(" stop touching me");
      bri++;
    }
  }

  delay(100);                             // arbitrary delay to limit data to serial port
}

void messageReceived(String& topic, String& payload) {

  if (topic == "arduino") {

    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(payload);

    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }

  }
}
