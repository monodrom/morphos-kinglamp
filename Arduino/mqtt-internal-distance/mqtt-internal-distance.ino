/*
 * needed for this to work is the kingslamp noodl project
 * distance sensor and three buttons
 */

#include <WiFi101.h>
#include <MQTTClient.h>
#include <ArduinoJson.h>

const char *wifi_ssid = "NETGEAR32"; //replace with your wifi name
const char *wifi_pass = "stronghat423"; //replace with your wifi password

const char *mqtt_broker = "10.0.0.3"; //replace with the IP of the computer running Noodl
const char *mqtt_name = "My Arduino"; //You can change this to be anything

const char *mqtt_id = ""; //token key/username. Noodl doesn't require authentication so leave this blank
const char *mqtt_password = ""; //token secret/password. Noodl doesn't require authentication so leave this blank

//three buttons
#define inPin 6 // on/off button
#define inPin2 5 // ct --
#define inPin3 7 // ct ++

//distance sensor
#define trigPin 4
#define echoPin 3

int state = HIGH;
int reading;
int previous = LOW;
int state2 = HIGH;
int reading2;
int previous2 = LOW;
int state3 = HIGH;
int reading3;
int previous3 = LOW;

int ct = 327;
//int ct;

long time = 0;
long time2 = 0;
long time3 = 0;
long debounce = 200;

long duration, distance, olddistance;

long previousMillis = 0;
long interval = 200;

WiFiClient wifi_client; //Tip: replace this with WiFiSSLClient if you use another broker that requires SSL
MQTTClient mqtt_client;

void messageReceived(String& topic, String& payload);

void connectToWifi() {
  Serial.print("connecting wifi...");

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");
}

void connectToBroker() {
  Serial.print("\nconnecting mqtt...");
  while (!mqtt_client.connect(mqtt_name, mqtt_id, mqtt_password)) {
    Serial.print(".");
  }

  Serial.println("\nconnected!");
}


void setup() {
  Serial.begin(115200);

  WiFi.begin(wifi_ssid, wifi_pass);
  mqtt_client.begin(mqtt_broker, wifi_client);
  mqtt_client.onMessage(messageReceived);

  pinMode(inPin, INPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

}

void loop() {
  if (!mqtt_client.connected()) {
    connectToWifi();
    connectToBroker();
    mqtt_client.subscribe("arduino");
  }

  //if there's an incoming message, the messageReceived callback function will be called
  mqtt_client.loop();


  //read button state and toggle
  reading = digitalRead(inPin);

  //toggle between on and off
  if (reading == HIGH && previous == LOW && millis() - time > debounce) {
    if (state == HIGH) {
      state = LOW;
      mqtt_client.publish("arduino", "{\"high\": 0}");
      Serial.println("zero");
    }
    else {
      state = HIGH;
      mqtt_client.publish("arduino", "{\"high\": 1}");
      Serial.println("one");
    }
    time = millis();
  }

    reading2 = digitalRead(inPin2);

//increase ct so warmer light
  if (reading2 == HIGH && previous2 == LOW && millis() - time2 > debounce) {
    if (state2 == HIGH) {
      state2 = LOW;
      ct = ct + 10;
      String payload = "{\"ct\": " ;
      String einde = "}" ;
      String message = payload + ct + einde;
      mqtt_client.publish("arduino", message );
      Serial.print(ct);
      Serial.println(" ct");
    }
    else {
      state2 = HIGH;
      ct = ct + 10;
      String payload = "{\"ct\": " ;
      String einde = "}" ;
      String message = payload + ct + einde;
      mqtt_client.publish("arduino", message );
      Serial.print(ct);
      Serial.println(" ct");
    }
    time2 = millis();
  }

//decrease ct so colder light
  reading3 = digitalRead(inPin3);
    if (reading3 == HIGH && previous3 == LOW && millis() - time3 > debounce) {
    if (state3 == HIGH) {
      state3 = LOW;
      ct = ct - 10;
      String payload = "{\"ct\": " ;
      String einde = "}" ;
      String message = payload + ct + einde;
      mqtt_client.publish("arduino", message );
      Serial.print(ct);
      Serial.println(" ct");
    }
    else { 
      state3 = HIGH;
      ct = ct - 10;
      String payload = "{\"ct\": " ;
      String einde = "}" ;
      String message = payload + ct + einde;
      mqtt_client.publish("arduino", message );
      Serial.print(ct);
      Serial.println(" ct");
    }
    time3 = millis();
  }


//distance sensing in cm remapped in noodl for brightness
  previous = reading;
  previous2 = reading2;
  previous3 = reading3;

  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    duration = pulseIn(echoPin, HIGH);
    distance = (duration / 2) / 29.1;
    if (distance <= 30 && distance >= 0 && distance == olddistance) {
      Serial.print(distance);
      Serial.println(" second");
      String payload = "{\"bri\": " ;
      String einde = "}" ;
      String message = payload + distance + einde;
      mqtt_client.publish("arduino", message );
    }
    else if (distance <= 30 && distance >= 0) {
      olddistance = distance;
      Serial.print(distance);
      Serial.println(" first");
    }
  }

  //Serial.println(ct);
}

void messageReceived(String& topic, String& payload) {

  if (topic == "arduino") {
//    Serial.println("neopixel do stuff");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(payload);

    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }

//recieve ct value if changed in noodl app
    ct = root["ctn"].as<int>();
    //Serial.println(ct);
  }
}





