/*
 * needed for this to work is the arduino input noodl project
 * reads the signal of every input pin and sends it to noodl as Pin + number for digital pins
 * and Pin + A + number for analog pins
 */

#include <WiFi101.h>
#include <MQTTClient.h>
#include <ArduinoJson.h>

const char *wifi_ssid = "NETGEAR32"; //replace with your wifi name
const char *wifi_pass = "stronghat423"; //replace with your wifi password

const char *mqtt_broker = "10.0.0.4"; //replace with the IP of the computer running Noodl
const char *mqtt_name = "My Arduino"; //You can change this to be anything

const char *mqtt_id = ""; //token key/username. Noodl doesn't require authentication so leave this blank
const char *mqtt_password = ""; //token secret/password. Noodl doesn't require authentication so leave this blank

WiFiClient wifi_client; //Tip: replace this with WiFiSSLClient if you use another broker that requires SSL
MQTTClient mqtt_client;

int delta = 100;
int oldA0, oldA1, oldA2, oldA3, oldA4, oldA5, oldA6;
byte APins[] = {oldA0, oldA1, oldA2, oldA3, oldA4, oldA5, oldA6};

int old0, old1, old2, old3, old4, old5, old6, old7, old8, old9, old10, old11, old12, old13, old14;
byte DPins[] = {old0, old1, old2, old3, old4, old5, old6, old7, old8, old9, old10, old11, old12, old13, old14};

void messageReceived(String& topic, String& payload);

void connectToWifi() {
  Serial.print("connecting wifi...");

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");
}

void connectToBroker() {
  Serial.print("\nconnecting mqtt...");
  while (!mqtt_client.connect(mqtt_name, mqtt_id, mqtt_password)) {
    Serial.print(".");
  }

  Serial.println("\nconnected!");
}


void setup() {
  Serial.begin(115200);

  WiFi.begin(wifi_ssid, wifi_pass);
  mqtt_client.begin(mqtt_broker, wifi_client);
  //  mqtt_client.onMessage(messageReceived);

  //set all digital pins to input
  for (int i = 0; i < 14; i++) {
    pinMode(i, INPUT);
  }
}

void loop() {
  if (!mqtt_client.connected()) {
    connectToWifi();
    connectToBroker();
    //    mqtt_client.subscribe("arduino");
  }

  //if there's an incoming message, the messageReceived callback function will be called

  mqtt_client.loop();
  
  for (int i = 0; i < 14; i++) {
    if (digitalRead(i) != DPins[i]) {
      DPins[i] = digitalRead(i);
//      Serial.print(digitalRead(i));
//      Serial.print(DPins[i]);
//      Serial.println(i);
      String payload1 = "{\"pin";
      String payload2 = "\": ";
      String einde = "}" ;
      String message = payload1 + i + payload2 + digitalRead(i) + einde; //send pinI, value to MQTT
      mqtt_client.publish("arduino", message );
    }
  }

  for (int i = 0; i < 6; i++) {
    int diff = abs(analogRead(i) - APins[i]);
    if (diff > delta) {
      APins[i] = analogRead(i);
      //fix with http://forum.arduino.cc/index.php?topic=43387.0
      String payload1 = "{\"pinA";
      String payload2 = "\": ";
      String einde = "}" ;
      String message = payload1 + i + payload2 + analogRead(i) + einde; //send pinI, value to MQTT
      mqtt_client.publish("arduino", message );
    }

  }

}





