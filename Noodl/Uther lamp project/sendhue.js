define({
    inputs: {
        tryToReachBrokerService: 'boolean',
    },
    outputs: {
        ip: 'string',
        foundIp: 'boolean',
        timeout: 'boolean',
        foundNoLights: 'boolean'
    },
    setup: function(inputs, outputs) {
        var self = this;

        function handleResponse()
        {
            var responseData = JSON.parse(this.responseText);
            if(responseData.length > 0)
            {
                var localIp = responseData[0].internalipaddress;
                outputs.ip = localIp;
                self.sendSignalOnOutput('foundIp');
            }
            else
            {
                self.sendSignalOnOutput('foundNoLights');
            }

        }

        function handleError()
        {
            self.sendSignalOnOutput('timeout');
        }

        this.xmlHttp = new window.XMLHttpRequest();
        this.xmlHttp.timeout = 1000;
        this.xmlHttp.addEventListener("load", handleResponse);
        this.xmlHttp.addEventListener("error", handleError);
        this.xmlHttp.addEventListener("abort", handleError);

    },
    run: function(inputs, outputs) {
        var self = this;

        if(inputs.tryToReachBrokerService === true)
        {
            var asynchRequest = true;
            self.xmlHttp.open("GET", 'https://www.meethue.com/api/nupnp', asynchRequest);
            self.xmlHttp.send();
        }
    }
});
